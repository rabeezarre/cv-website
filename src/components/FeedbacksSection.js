import React from "react";
import Box from "./shared/Box";
import Feedbacks from "./shared/Feedbacks";
import avatar from "../assets/images/feedback_user.png";

const data = [
  {
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.",
    user: {
      photo: avatar,
      name: "Martin Friman Programmer",
      cite: "https://www.citeexample.com",
    },
  },
  {
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.",
    user: {
      photo: avatar,
      name: "Martin Friman Programmer",
      cite: "https://www.citeexample.com",
    },
  },
];

export default function FeedbacksSection() {
  return (
    <section id="feedbacks">
      <Box title="Feedbacks">
        <Feedbacks data={data} />
      </Box>
    </section>
  );
}
