import React from "react";
import Box from "./shared/Box";
import Address from "./shared/Address";
import { faPhone, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faTwitter,
  faFacebookF,
  faSkype,
} from "@fortawesome/free-brands-svg-icons";

const data = [
  {
    name: "500 342 242",
    url: "tel:+500 342 242",
    icon: faPhone,
    isSingleLine: true,
  },
  {
    name: "office@kamsolutions.pl",
    url: "mailto:office@kamsolutions.pl",
    icon: faEnvelope,
    isSingleLine: true,
  },
  {
    name: "Twitter",
    url: "https://twitter.com/wordpress",
    icon: faTwitter,
    isSingleLine: false,
  },
  {
    name: "Facebook",
    url: "https://www.facebook.com/facebook",
    icon: faFacebookF,
    isSingleLine: false,
  },
  {
    name: "Skype",
    url: "kamsolutions.pl",
    icon: faSkype,
    isSingleLine: false,
  },
];

export default function ContactSection() {
  return (
    <section id="contact">
      <Box title="Contact">
        <Address data={data} />
      </Box>
    </section>
  );
}
