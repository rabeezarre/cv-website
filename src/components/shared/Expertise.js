import React from "react";

export default function Expertise({data}) {
  return (
    <div className="expertise-container">
      {data.map((item, index) => (
        <div className="expertise-container__item" key={index}>
          <div className="expertise-container__header">
            <span className="expertise-container__company">
              {item.info.company}
            </span>
            <span className="expertise-container__date">{item.date}</span>
          </div>
          <div className="expertise-container__content">
            <span className="expertise-container__content-job">
              {item.info.job}
            </span>
            <span className="expertise-container__content-description">
              {item.info.description}
            </span>
          </div>
        </div>
      ))}
    </div>
  );
}
