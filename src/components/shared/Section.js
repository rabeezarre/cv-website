import React from 'react'

export default function Section({ header, content }) {
  return (
    <section className="section-container">
        <span className="section-container-header">{header}</span>
        <div className="section-container-content">{content}</div>
    </section>
  )
}
