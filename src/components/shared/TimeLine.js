import React from "react";
import "../../assets/styles/TimeLineStyle.scss"

export default function TimeLine({ data }) {
  return (
    <div className="timeline-container">
      {data.map((item, index) => (
        <div className="timeline-container__item" key={index}>
          <span className="timeline-container__date">{item.date}</span>
          <div className="timeline-container__content">
            <span className="timeline-container__content-title">{item.title}</span>
            <span className="timeline-container__content-text">{item.text}</span>
          </div>
        </div>
      ))}
    </div>
  );
}
