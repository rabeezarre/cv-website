import React from "react";
import "../../assets/styles/PanelPhotoBoxStyle.scss";
import PhotoBox from "./PhotoBox";

export default function PanelPhotoBox({ img, alt, title }) {
  return (
    <div className="photobox-container">
      <PhotoBox img={img} alt={alt} size={"md"} collapsable={true} />
      <span className="title">{title}</span>
    </div>
  );
}
