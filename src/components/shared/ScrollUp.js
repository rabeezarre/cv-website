import React from "react";
import "../../assets/styles/ScrollUpStyle.scss";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";
import SmallButton from "./SmallButton";

export default function ScrollUp() {
  function handleScrollUp() {
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
  }

  return (
    // <div className="scroll-up-button" onClick={() => handleScrollUp()}>
    //   <FontAwesomeIcon icon={faChevronUp} color="white" />
    // </div>
    <div className="scroll-up-button">
      <SmallButton icon={faChevronUp} func={handleScrollUp} direction={"up"}/>
    </div>
    
  );
}
