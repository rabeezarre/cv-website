import React, { useState } from "react";
import "../../assets/styles/PortfolioCardStyle.scss";

const PortfolioCard = ({ data }) => {
  const { name, description, link, img } = data;
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <div
      className={`portfolio-card ${isHovered ? "hovered" : ""}`}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className="portfolio-card__image">
        <img src={img} alt={name} />
        <div className="portfolio-card__overlay">
          <div className="portfolio-card__content">
            <span className="portfolio-card__title">{name}</span>
            <span className="portfolio-card__description">{description}</span>
            <span className="portfolio-card__link">
              <a href={link} target="_blank" rel="noopener noreferrer">
                Visit resource
              </a>
            </span>
            {/* <div className="portfolio-card__tags">
              {type.map((tag, index) => (
                <span key={index} className="portfolio-card__tag">
                  {tag}
                </span>
              ))}
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PortfolioCard;
