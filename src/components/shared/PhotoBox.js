import React from "react";
import "../../assets/styles/PhotoBoxStyle.scss"

export default function PhotoBox({ img, alt, size, collapsable }) {
    const getCollapsibleClassName = () => {
        return collapsable ? "collapsable" : "";
    };

  return (
    <div className={`circle-container ${size} ${getCollapsibleClassName()}`}>
      <img className="circle-image" src={img} alt={alt} />
    </div>
  );
}
