import React from "react";
import Button from "./Button";
import "../../assets/styles/BoxStyle.scss"

const Box = ({ title, children, button }) => {
  return (
    <div className="box-container">
      <div className="box-container__header">
        <span className="box-container__title">{title}</span>
        {button && <Button icon={button.icon} text={button.text} func={button.func}/>}
      </div>
      <div className="box-container__content">{children}</div>
    </div>
  );
};

export default Box;
