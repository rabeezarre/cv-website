import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faPen,
  faSuitcase,
  faGraduationCap,
  faLocationArrow,
} from "@fortawesome/free-solid-svg-icons";
import "../../assets/styles/NavigationStyle.scss";
import { faComment, faGem } from "@fortawesome/free-regular-svg-icons";

const menuItems = [
  { title: "About me", link: "about-me", icon: faUser },
  { title: "Education", link: "education", icon: faGraduationCap },
  { title: "Experience", link: "experience", icon: faPen },
  { title: "Skills", link: "skills", icon: faGem },
  { title: "Portfolio", link: "portfolio", icon: faSuitcase },
  { title: "Contact", link: "contact", icon: faLocationArrow },
  { title: "Feedbacks", link: "feedbacks", icon: faComment },
];

const Navigation = () => {
  const [activeSection, setActiveSection] = useState("");

  useEffect(() => {
    const handleScroll = () => {
      const sections = document.querySelectorAll("section");
      const scrollPosition = window.pageYOffset;

      for (let i = sections.length - 1; i >= 0; i--) {
        const section = sections[i];
        const sectionTop = section.offsetTop;

        if (scrollPosition >= sectionTop - 270) {
          setActiveSection(section.id);
          break;
        }
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <nav>
      <ul className="navigation-container">
        {menuItems.map((item, index) => (
          <li
            key={index}
            className={activeSection === item.link ? "active" : ""}
          >
            <a href={"#" + item.link}>
              <div className="navigation-container-item">
                <div className="navigation-container-item__icon-wrapper">
                  <FontAwesomeIcon
                    icon={item.icon}
                    className={`navigation-container-item__icon ${
                      activeSection === item.link ? "active" : ""
                    }`}
                    color="#667081"
                  />
                </div>
                <span
                  className={`navigation-container-item__title ${
                    activeSection === item.link ? "active" : ""
                  }`}
                >
                  {item.title}
                </span>
              </div>
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navigation;
