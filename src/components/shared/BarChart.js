import React from "react";
import "../../assets/styles/BarChartStyle.scss";
import { useSelector } from "react-redux";

const BarChart = () => {
  const data = useSelector((state) => state.skills.formData);
  const maxValue = 100;

  return (
    <div className="bar-chart-container">
      {data.map((item) => (
        <div className="bar-container" key={item.label}>
          <div
            className="bar"
            style={{
              width: `${(item.value / maxValue) * 100}%`,
            }}
          >
            <span className="bar-label">{`${item.label}`}</span>
          </div>
        </div>
      ))}
      <div className="bar-chart-labels">
        <div className="line">
          <div className="point" id="point1"></div>
          <div className="point" id="point2"></div>
          <div className="point" id="point3"></div>
          <div className="point" id="point4"></div>
        </div>
        <div className="labels-container">
          <span className="label" id="point1">
            Beginner
          </span>
          <span className="label" id="point2">
            Proficient
          </span>
          <span className="label" id="point3">
            Expert
          </span>
          <span className="label" id="point4">
            Master
          </span>
        </div>
      </div>
    </div>
  );
};

export default BarChart;
