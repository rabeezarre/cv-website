import React from 'react'
import "../../assets/styles/ErrorMessageStyle.scss"

export default function ErrorMessage({children}) {
  return (
    <div className="error-message">
        {children}
    </div>
  )
}
