import React, { useState } from "react";
import PortfolioCard from "./PortfolioCard";
import "../../assets/styles/PortfolioStyle.scss";

export default function Portfolio({ data }) {
  const [selectedType, setSelectedType] = useState("All");

  const handleFilterChange = (type) => {
    setSelectedType(type);
  };

  const filteredData =
    selectedType === "All"
      ? data
      : data.filter((item) => item.type.includes(selectedType));

  return (
    <div className="portfolio-outer-container">
      <div className="portfolio-filter">
        <span onClick={() => handleFilterChange("All")} className={selectedType === "All" ? "filter-active" : ""}>All</span> / 
        <span onClick={() => handleFilterChange("Code")} className={selectedType === "Code" ? "filter-active" : ""}> Code</span> / 
        <span onClick={() => handleFilterChange("UI")} className={selectedType === "UI" ? "filter-active" : ""}> UI</span>
      </div>
      <div className="portfolio-container">
        {filteredData.map((item, index) => (
          <PortfolioCard key={index} data={item} />
        ))}
      </div>
    </div>
  );
}
