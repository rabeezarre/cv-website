import React from 'react'
import "../../assets/styles/CustomInputStyle.scss"

export default function CustomInput(props) {
  return (
    <input className="custom-input" type="text" {...props} />
  )
}
