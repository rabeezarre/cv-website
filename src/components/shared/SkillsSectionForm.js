import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import "../../assets/styles/SkillsSectionFormStyle.scss";
import CustomInput from "./CustomInput";
import Button from "./Button";
import { useDispatch } from "react-redux";
import { addSkill } from "../../redux/skillsSlice";

export default function SkillsSectionForm() {
  const dispatch = useDispatch();

  const handleSubmit = (values, { setSubmitting }) => {
    setTimeout(() => {
      const item = { label: values.skill, value: parseInt(values.range) };
      dispatch(addSkill(item));
      setSubmitting(false);
    }, 400);
  };

  return (
    <div className="form-container">
      <Formik
        initialValues={{ skill: "", range: "" }}
        validate={(values) => {
          const errors = {};

          if (!values.skill) {
            errors.skill = "Skill name is required field";
          }

          if (!values.range) {
            errors.range = "Skill range is required field ";
          } else if (values.range > 100) {
            errors.range = "Skill range must be less than or equal 100";
          } else if (values.range < 10) {
            errors.range = "Skill range must be greater than or equal 10";
          }else if (isNaN(values.range)) {
            errors.range = "Skill range must be a number";
          }

          return errors;
        }}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, isValid, dirty }) => (
          <Form className="form-input-container">
            <div className="form-input">
              <div className="form-input-inner">
                <span>Skill name:</span>
                <Field
                  type="string"
                  name="skill"
                  placeholder="Enter skill name"
                  as={CustomInput}
                />
              </div>
              <ErrorMessage
                name="skill"
                component="div"
                className="error-container"
              />
            </div>
            <div className="form-input">
              <div className="form-input-inner">
                <span>Skill range:</span>
                <Field
                  type="string"
                  name="range"
                  placeholder="Enter skill range"
                  as={CustomInput}
                />
              </div>
              <ErrorMessage name="range">
                {(msg) => <div className="error-container">{msg}</div>}
              </ErrorMessage>
            </div>
            <div>
              <Button
                text="Add skill"
                type="submit"
                disabled={isSubmitting || !isValid || !dirty}
              />
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
