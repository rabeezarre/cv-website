import React from "react";
import PhotoBox from "./PhotoBox";
import Info from "./Info";

const Feedbacks = ({ data }) => {
  return (
    <div className="feedbacks-container">
      {data.map((feedback, index) => (
        <div className="feedbacks-container-item" key={index}>
          <Info text={feedback.text} />
          <div className="feedbacks-container-item__img-name">
            <PhotoBox
              img={feedback.user.photo}
              alt="User Feedback"
              size={"sm"}
            />
            <div className="feedbacks-container-item__name">
              <span>
                {feedback.user.name},{" "}
                <a href={feedback.user.cite}>{feedback.user.cite.replace(/^https?:\/\/(www\.)?/, '')}</a>
              </span>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Feedbacks;
