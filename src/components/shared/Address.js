import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Address = ({ data }) => {
  return (
    <div className="address-container">
      {data.map((item, index) => (
        <div className="address-container__item" key={index}>
          <div className="address-container__item-icon">
            <FontAwesomeIcon icon={item.icon} size="2x" color="#26c17e" />
          </div>
          {item.isSingleLine ? (
            <div className="address-container__item-description">
              <span>
                <a href={item.url}>{item.name}</a>
              </span>
            </div>
          ) : (
            <div className="address-container__item-description">
              <span>{item.name}</span>
              <a href={item.url}>{item.url}</a>
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default Address;
