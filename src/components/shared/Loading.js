import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import "../../assets/styles/LoadingStyle.scss";

export default function Loading() {
  return (
    <div className="loading-overlay">
      <div className="loading-content">
        <FontAwesomeIcon className="loading-icon" icon={faSyncAlt} spin />
      </div>
    </div>
  );
}
