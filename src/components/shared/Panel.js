import React, { useState } from "react";
import Navigation from "./Navigation";
import Button from "./Button";
import { faChevronLeft, faBars } from "@fortawesome/free-solid-svg-icons";
import userAvatar from "../../assets/images/user_avatar.png";
import PanelPhotoBox from "./PanelPhotoBox";
import "../../assets/styles/PanelStyle.scss";
import { Link } from "react-router-dom";
import SmallButton from "./SmallButton";

export default function Panel() {
  const [isCollapsed, setIsCollapsed] = useState(false);

  const handleCollapse = () => {
    setIsCollapsed(!isCollapsed);
  };

  return (
    <div className="panel-outer-container">
      <div className="collapse-button">
        <SmallButton icon={faBars} func={handleCollapse} direction={"right"}/>
      </div>
      
     
      <div className={`panel-container ${isCollapsed ? "collapsed" : ""}`}>
        {!isCollapsed && (
          <div className="panel-content">
            <div className="panel-content__user">
              <PanelPhotoBox img={userAvatar} alt="User" title="John Doe" />
            </div>
            <div className="panel-content__nav">
              <Navigation />
            </div>
            <div className="panel-content__button">
              <Link to="/">
                <Button text="Go back" icon={faChevronLeft} />
              </Link>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
