import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../../assets/styles/SmallButtonStyle.scss"

export default function SmallButton({icon, func, direction}) {
  return (
    <div className={`small-button ${direction}`} onClick={func}>
      <FontAwesomeIcon icon={icon} color="white" />
    </div>
  )
}
