import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../../assets/styles/ButtonStyle.scss";

export default function Button({ icon, text, func, type, disabled }) {
  return (
    <button className={`button ${disabled ? "disabled" : ""}`} onClick={func} type={type ? type : ""}>
      {icon ? (
        <>
          <FontAwesomeIcon icon={icon} className="icon" />
          <span className="text-with-icon">{text}</span>
        </>
      ) : (
        <span>{text}</span>
      )}
    </button>
  );
}
