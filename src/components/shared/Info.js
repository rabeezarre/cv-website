import React from "react";
import "../../assets/styles/InfoStyle.scss"

export default function Info({ text }) {
  return <div className="info-container">{text}</div>;
}
