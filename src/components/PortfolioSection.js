import React from "react";
import Box from "./shared/Box";
import Portfolio from "./shared/Portfolio";
import project1 from "../assets/images/project1.png";
import project2 from "../assets/images/project2.png";
import project3 from "../assets/images/project3.png";

const data = [
  {
    name: "Project",
    description:
      "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis",
    link: "https://www.project.com",
    img: project1,
    type: ["UI"],
  },
  {
    name: "Project",
    description:
      "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis",
    link: "https://www.project.com",
    img: project2,
    type: ["UI"],
  },
  {
    name: "Project",
    description:
      "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis",
    link: "https://www.project.com",
    img: project3,
    type: ["UI", "Code"],
  },
  {
    name: "Project",
    description:
      "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis",
    link: "https://www.project.com",
    img: project1,
    type: ["Code"],
  },
];

export default function PortfolioSection() {
  return (
    <section id="portfolio">
      <Box title="Portfolio">
        <Portfolio data={data} />
      </Box>
    </section>
  );
}
