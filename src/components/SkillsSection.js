import React from "react";
import Box from "./shared/Box";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import BarChart from "./shared/BarChart";
import SkillsSectionForm from "./shared/SkillsSectionForm";
import { useSelector, useDispatch } from "react-redux";
import { openCloseSection } from "../redux/skillsSlice";

export default function SkillsSection() {
  const isSectionOpen = useSelector((state) => state.skills.isSectionOpen);
  const dispatch = useDispatch();

  const buttonFunc = () => {
    dispatch(openCloseSection());
  };

  const button = {
    text: "Open Edit",
    icon: faPen,
    func: buttonFunc,
  };

  return (
    <section id="skills">
      <Box title="Skills" button={button}>
        {isSectionOpen && <SkillsSectionForm />}
        <BarChart />
      </Box>
    </section>
  );
}
