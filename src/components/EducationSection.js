import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchEducations } from "../redux/educationSlice";
import Box from "./shared/Box";
import TimeLine from "./shared/TimeLine";
import Loading from "./shared/Loading";
import ErrorMessage from "./shared/ErrorMessage";

export default function EducationSection() {
  const dispatch = useDispatch();
  const educations = useSelector((state) => state.education.educations);
  const loading = useSelector((state) => state.education.loading);

  useEffect(() => {
    dispatch(fetchEducations());
  }, [dispatch]);

  return (
    <section id="education">
      <Box title="Education">
        {loading ? (
          <Loading />
        ) : educations ? (
          <TimeLine data={educations} />
        ) : (
          <ErrorMessage>
            Something went wrong; please review your server connection!
          </ErrorMessage>
        )}
      </Box>
    </section>
  );
}
