import React from "react";
import AboutMeSection from "./AboutMeSection";
import EducationSection from "./EducationSection";
import ExperienceSection from "./ExperienceSection";
import SkillsSection from "./SkillsSection";
import ContactSection from "./ContactSection";
import FeedbacksSection from "./FeedbacksSection";
import PortfolioSection from "./PortfolioSection";

export default function MainContent() {
  return (
    <main className="main-content-container">
      <AboutMeSection/>
      <EducationSection/>
      <ExperienceSection/>
      <SkillsSection/>
      <PortfolioSection/>
      <ContactSection/>
      <FeedbacksSection/>
    </main>
  );
}
