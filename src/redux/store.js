import { configureStore } from "@reduxjs/toolkit";
import skillsReducer from "./skillsSlice";
import educationReducer from "./educationSlice";
import { composeWithDevTools } from "@redux-devtools/extension";
import localStorageMiddleware from "./localStorageMiddleware";

export const store = configureStore({
  reducer: {
    skills: skillsReducer,
    education: educationReducer,
  },
  enhancers: { composeWithDevTools },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(localStorageMiddleware),
});
