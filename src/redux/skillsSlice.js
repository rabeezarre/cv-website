import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const storedFormData = JSON.parse(localStorage.getItem("formData"));

const initialState = {
  isSectionOpen: false,
  formData: storedFormData || [],
  loading: false,
  error: null,
};

export const fetchSkills = createAsyncThunk("skills/fetch", async () => {
  const response = await axios.get("/api/skills");
  return response.data;
});

export const skillsSlice = createSlice({
  name: "skills",
  initialState,
  reducers: {
    openCloseSection: (state) => {
      state.isSectionOpen = !state.isSectionOpen;
    },
    addSkill: (state, action) => {
      state.formData.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchSkills.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchSkills.fulfilled, (state, action) => {
        state.loading = false;
        state.skills = action.payload;
      })
      .addCase(fetchSkills.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const { openCloseSection, addSkill } = skillsSlice.actions;

export default skillsSlice.reducer;
