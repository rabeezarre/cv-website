const localStorageMiddleware = (store) => (next) => (action) => {
  if (action.type === "skills/addSkill") {
    const { formData } = store.getState().skills;
    localStorage.setItem("formData", JSON.stringify(formData));
  }
  return next(action);
};

export default localStorageMiddleware;
