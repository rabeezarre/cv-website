import React from 'react'
import MainContent from '../components/MainContent'
import Panel from '../components/shared/Panel'
import ScrollUp from '../components/shared/ScrollUp'

export default function MainPage() {
  return (
    <div className='main-page-container'>
      <Panel/>
      <MainContent/>
      <ScrollUp/>
    </div>
  )
}
