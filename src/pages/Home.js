import React from "react";
import userAvatar from "../assets/images/user_avatar.png";
import { Link } from "react-router-dom";
import Button from "../components/shared/Button";
import PhotoBox from "../components/shared/PhotoBox";

export default function Home() {
  return (
    <div className="home-container">
      <div className="home-inner-container">
        <PhotoBox img={userAvatar} alt="User" size={"lg"} />
        <span className="home-inner-container__name">John Doe</span>
        <span className="home-inner-container__position">
          Programmer. Creative. Innovator
        </span>
        <span className="home-inner-container__description">
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
          commodo ligula eget dolor. Aenean massa. Cum sociis natoque
        </span>
        <Link to="/main">
          <Button text="Know more" />
        </Link>
      </div>
    </div>
  );
}
