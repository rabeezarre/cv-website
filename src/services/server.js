import { Server, Model, Response, Factory } from "miragejs";

export function makeServer({ environment = "development" } = {}) {
  const server = new Server({
    models: {
      education: Model,
      skill: Model,
    },
    factories: {
      education: Factory.extend({
        date() {
          let min = 1950;
          let max = 2023;

          return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        title(i) {
          return `Title ${i}`;
        },
        text() {
          const strings = [
            "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.",
            "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.",
          ];

          const randomIndex = Math.floor(Math.random() * strings.length);
          return strings[randomIndex];
        },
      }),
      skill: Factory.extend({
        label(i) {
          const strings = ["HTML", "CSS", "Java", "C++", "React"];
          const index = i % strings.length;
          return strings[index];
        },
        range() {
          let min = 10;
          let max = 100;

          return Math.floor(Math.random() * (max - min + 1)) + min;
        },
      }),
    },

    routes() {
      this.namespace = "/api";

      this.get(
        "/educations",
        (schema) => {
          return schema.educations.all();
        },
        { timing: 4000 }
      );

      // this.get(
      //   "/educations",
      //   () => {
      //       return new Response(500, { some: 'Error' }, { errors: [ 'Internal Error'] });
      //     },
      //   { timing: 4000 }
      // );

      this.get(
        "/skills",
        (schema) => {
          return schema.skills.all();
        },
        { timing: 0 }
      );

      this.post("/skills", (schema, request) => {
        const attrs = JSON.parse(request.requestBody);
        return schema.skills.create(attrs);
      });
    },
    seeds(server) {
      server.createList("education", 3);
      server.createList("skill", 5);
    },
  });

  return server;
}
