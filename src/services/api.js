const API_BASE_URL = '/api';

async function handleResponse(response) {
  if (!response.ok) {
    throw new Error('Request failed');
  }
  const data = await response.json();
  return data;
}

async function fetchEducations() {
  try {
    const response = await fetch(`${API_BASE_URL}/educations`);
    const educations = await handleResponse(response);
    return educations;
  } catch (error) {
    console.error(error);
    return [];
  }
}

async function fetchSkills() {
  try {
    const response = await fetch(`${API_BASE_URL}/skills`);
    const skills = await handleResponse(response);
    return skills;
  } catch (error) {
    console.error(error);
    return [];
  }
}

async function createSkill(skill) {
  try {
    const response = await fetch(`${API_BASE_URL}/skills`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(skill),
    });
    const createdSkill = await handleResponse(response);
    return createdSkill;
  } catch (error) {
    console.error(error);
    return null;
  }
}

export { fetchEducations, fetchSkills, createSkill };
