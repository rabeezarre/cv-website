# CV Website

A personal CV website built with **React**, showcasing my proficiency in building dynamic, responsive, and scalable web applications. This project highlights my skills in **Redux**, **React Hooks**, **services integration**, and efficient state management.

## Features

- **Redux** for seamless state management.
- **React Hooks** (useState, useEffect) for functional and efficient components.
- **Custom Services** for handling API calls and modular architecture.
- **Responsive Design** ensuring compatibility across all devices.
- Modular and reusable **React components**.
- Clean and maintainable code structure.

## Technologies Used

- **React**: Frontend library for UI.
- **Redux**: State management.
- **React Router**: Navigation and routing.
- **CSS/SCSS**: Styling the application.
- **JavaScript/ES6**: Core scripting.

## Getting Started

Follow these steps to run the project locally:

### Prerequisites
Ensure you have the following installed:
- Node.js and npm/yarn

### Installation
1. Clone the repository:
   ```bash
   git clone https://gitlab.com/rabeezarre/cv-website.git
   ```
2. Navigate to the project directory:
   ```bash
   cd cv-website
   ```
3. Install dependencies:
   ```bash
   npm install
   ```

### Running the Project
To start the development server:
```bash
npm start
```

### Building the Project
To create a production build:
```bash
npm run build
```

## Key Highlights in Code

### Redux Integration
Example of a Redux slice:
```javascript
import { createSlice } from '@reduxjs/toolkit';

const exampleSlice = createSlice({
  name: 'example',
  initialState: { value: 0 },
  reducers: {
    increment: (state) => { state.value += 1; },
    decrement: (state) => { state.value -= 1; },
  },
});

export const { increment, decrement } = exampleSlice.actions;
export default exampleSlice.reducer;
```

### Service Integration
Example of an API service:
```javascript
import axios from 'axios';

export const fetchUserData = async () => {
  try {
    const response = await axios.get('/api/user');
    return response.data;
  } catch (error) {
    console.error('Error fetching user data:', error);
    throw error;
  }
};
```

### React Hook Example
```javascript
import React, { useState, useEffect } from 'react';

const ExampleComponent = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetchUserData().then(setData).catch(console.error);
  }, []);

  return (
    <div>
      <h1>Data:</h1>
      {data ? <pre>{JSON.stringify(data, null, 2)}</pre> : 'Loading...'}
    </div>
  );
};
```

## Contributing
Feel free to fork this repository and make pull requests for improvements.

## License
This project is open-source and available under the [MIT License](LICENSE).
